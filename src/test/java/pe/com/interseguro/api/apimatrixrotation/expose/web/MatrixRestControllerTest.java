package pe.com.interseguro.api.apimatrixrotation.expose.web;

import static pe.com.interseguro.api.apimatrixrotation.util.Constants.ERROR_CODE;
import static pe.com.interseguro.api.apimatrixrotation.util.Constants.SUCCESS_CODE;
import static pe.com.interseguro.api.apimatrixrotation.util.Constants.URI_ROTATE;

import java.util.Arrays;

import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.WebTestClient;

import reactor.core.publisher.Mono;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class MatrixRestControllerTest {


  @Autowired
  private WebTestClient webTestClient;


  @Test
  public void testCaseOne() {

    Integer[][] arrayOfArrayNumbers = new Integer[][]{new Integer[]{1, 2}, new Integer[]{3, 4}};

    webTestClient.post()
        .uri(URI_ROTATE)
        .contentType(MediaType.APPLICATION_JSON_UTF8)
        .accept(MediaType.APPLICATION_JSON_UTF8)
        .body(Mono.just(arrayOfArrayNumbers), Integer[][].class)
        .exchange()
        .expectStatus().isOk()
        .expectHeader().contentType(MediaType.APPLICATION_JSON_UTF8)
        .expectBody()
        .jsonPath("$.code").isEqualTo(SUCCESS_CODE)
        .jsonPath("$.data").value(System.out::println)
        .jsonPath("$.data").value(Matchers.hasSize(2))
        .jsonPath("$.data[0]").value(Matchers.hasSize(2))
        .jsonPath("$.data[1]").value(Matchers.hasSize(2))
        .jsonPath("$.data[0]").value(Matchers.is(Arrays.asList(2, 4)))
        .jsonPath("$.data[1]").value(Matchers.is(Arrays.asList(1, 3)))
    ;

  }

  @Test
  public void testCaseTwo() {

    Integer[][] arrayOfArrayNumbers = new Integer[][]{new Integer[]{1, 2, 3}, new Integer[]{4, 5, 6}, new Integer[]{7, 8, 9}};

    webTestClient.post()
        .uri(URI_ROTATE)
        .contentType(MediaType.APPLICATION_JSON_UTF8)
        .accept(MediaType.APPLICATION_JSON_UTF8)
        .body(Mono.just(arrayOfArrayNumbers), Integer[][].class)
        .exchange()
        .expectStatus().isOk()
        .expectHeader().contentType(MediaType.APPLICATION_JSON_UTF8)
        .expectBody()
        .jsonPath("$.code").isEqualTo(SUCCESS_CODE)
        .jsonPath("$.data").value(System.out::println)
        .jsonPath("$.data").value(Matchers.hasSize(3))
        .jsonPath("$.data[0]").value(Matchers.hasSize(3))
        .jsonPath("$.data[1]").value(Matchers.hasSize(3))
        .jsonPath("$.data[2]").value(Matchers.hasSize(3))
        .jsonPath("$.data[0]").value(Matchers.is(Arrays.asList(3, 6, 9)))
        .jsonPath("$.data[1]").value(Matchers.is(Arrays.asList(2, 5, 8)))
        .jsonPath("$.data[2]").value(Matchers.is(Arrays.asList(1, 4, 7)))
    ;

  }

  @Test
  public void testCaseValidation() {

    Integer[][] arrayOfArrayNumbers = new Integer[][]{new Integer[]{1, 2}, new Integer[]{3, 4}, new Integer[]{5, 6}};

    webTestClient.post()
        .uri(URI_ROTATE)
        .contentType(MediaType.APPLICATION_JSON_UTF8)
        .accept(MediaType.APPLICATION_JSON_UTF8)
        .body(Mono.just(arrayOfArrayNumbers), Integer[][].class)
        .exchange()
        .expectStatus().isBadRequest()
        .expectHeader().contentType(MediaType.APPLICATION_JSON_UTF8)
        .expectBody()
        .jsonPath("@").value(System.out::println)
        .jsonPath("$.code").isEqualTo(ERROR_CODE)
        .jsonPath("$.message").isEqualTo("Not valid Matrix")
    ;

  }

}
