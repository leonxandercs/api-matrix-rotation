package pe.com.interseguro.api.apimatrixrotation.expose.web;

import static pe.com.interseguro.api.apimatrixrotation.util.Constants.URI_ROTATE;

import io.reactivex.Single;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import pe.com.interseguro.api.apimatrixrotation.rest.model.ResultModel;
import pe.com.interseguro.api.apimatrixrotation.service.IMatrixService;

@RestController
public class MatrixRestController {

  IMatrixService matrixService;

  @Autowired
  public MatrixRestController(IMatrixService matrixService) {
    this.matrixService = matrixService;
  }

  @PostMapping(value = URI_ROTATE)
  public Single<ResultModel<Integer[][]>> rotate(@RequestBody Integer[][] matrixNumber) {
    return matrixService.rotate(matrixNumber);

  }
}