package pe.com.interseguro.api.apimatrixrotation.util;


public interface Constants {

  String SUCCESS_CODE = "1000";
  String ERROR_CODE = "1001";


  String URI_ROTATE = "rotate";
}
