package pe.com.interseguro.api.apimatrixrotation;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiMatrixRotationApplication {

  public static void main(String[] args) {
    SpringApplication.run(ApiMatrixRotationApplication.class, args);
  }

}
