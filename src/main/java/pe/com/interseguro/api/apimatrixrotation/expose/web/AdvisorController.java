package pe.com.interseguro.api.apimatrixrotation.expose.web;

import lombok.extern.slf4j.Slf4j;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import pe.com.interseguro.api.apimatrixrotation.rest.model.ResultModel;
import pe.com.interseguro.api.apimatrixrotation.util.MapperUtil;

@RestControllerAdvice
@Slf4j
public class AdvisorController {


  @ExceptionHandler({Exception.class})
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  public ResultModel<?> handleException(Exception ex) {
    log.info("#handleException");

    return MapperUtil.toResultModel(ex);
  }


}
