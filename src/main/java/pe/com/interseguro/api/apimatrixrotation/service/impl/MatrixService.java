package pe.com.interseguro.api.apimatrixrotation.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;

import io.reactivex.Single;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.com.interseguro.api.apimatrixrotation.rest.model.ResultModel;
import pe.com.interseguro.api.apimatrixrotation.service.IMatrixService;
import pe.com.interseguro.api.apimatrixrotation.util.MapperUtil;
import pe.com.interseguro.api.apimatrixrotation.util.Utils;

@Service
@Slf4j
public class MatrixService implements IMatrixService {

  ObjectMapper mapper;

  @Autowired
  public MatrixService(ObjectMapper mapper) {
    this.mapper = mapper;
  }


  @Override
  public Single<ResultModel<Integer[][]>> rotate(Integer[][] matrixNumber) {
    return Single.just(matrixNumber)
        .map(Utils::validateMatrixNxN)
        .map(Utils::rotateMatrix)
        .map(MapperUtil::toResultModel)
        .doOnSubscribe(number -> log.info("subscribe"))
        .doOnError(error -> log.error(error.getMessage(), error))
        .doOnSuccess(arr -> log.debug("success rotate: {}", mapper.writeValueAsString(arr)));
  }
}
