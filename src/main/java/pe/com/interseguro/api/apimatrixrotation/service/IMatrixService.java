package pe.com.interseguro.api.apimatrixrotation.service;

import io.reactivex.Single;

import pe.com.interseguro.api.apimatrixrotation.rest.model.ResultModel;

public interface IMatrixService {

  Single<ResultModel<Integer[][]>> rotate(Integer[][] matrixNumber);

}
