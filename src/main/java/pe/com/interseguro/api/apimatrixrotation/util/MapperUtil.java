package pe.com.interseguro.api.apimatrixrotation.util;

import org.springframework.http.HttpStatus;

import pe.com.interseguro.api.apimatrixrotation.rest.model.ResultModel;

public final class MapperUtil {


  public static ResultModel<Integer[][]> toResultModel(Integer[][] matrix) {
    return ResultModel.<Integer[][]>builder()
        .status(HttpStatus.OK.value())
        .data(matrix)
        .code(Constants.SUCCESS_CODE)
        .title("message")
        .message("Successfully rotated")
        .build()
        ;
  }

  public static ResultModel<Integer[][]> toResultModel(Throwable error) {

    return ResultModel.<Integer[][]>builder()
        .status(HttpStatus.BAD_REQUEST.value())
        .code(Constants.ERROR_CODE)
        .title("ERROR")
        .message(error.getMessage())
        .build();
  }
}
