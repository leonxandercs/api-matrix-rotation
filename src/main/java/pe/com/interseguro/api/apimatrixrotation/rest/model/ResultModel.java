package pe.com.interseguro.api.apimatrixrotation.rest.model;


import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ResultModel<T> implements Serializable {

  private Integer status;
  private String code;
  private String title;
  private String message;
  private T data;


}
