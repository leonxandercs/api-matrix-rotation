package pe.com.interseguro.api.apimatrixrotation.util;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public final class Utils {


  public static Integer[][] validateMatrixNxN(final Integer[][] matrix) {

    List<Integer> sizeArrays = Arrays.stream(matrix)
        .map(array -> array.length)
        .distinct()
        .collect(Collectors.toList());

    boolean isSameSizeArrays = sizeArrays.size() == 1;

    boolean isValidMatrixNxN = isSameSizeArrays && ((double) matrix.length / sizeArrays.get(0)) == 1;

    if (isValidMatrixNxN) {
      return matrix;
    }

    throw new IllegalArgumentException("Not valid Matrix");
  }


  public static Integer[][] rotateMatrix(Integer[][] matrix) {
    for (Integer i = 0; i < matrix.length / 2; ++i) {
      for (Integer j = i; j < matrix.length - 1 - i; ++j) {

        int temp = matrix[j][i];

        matrix[j][i] = matrix[i][matrix.length - j - 1];
        matrix[i][matrix.length - j - 1] = matrix[matrix.length - j - 1][matrix.length - i - 1];
        matrix[matrix.length - j - 1][matrix.length - i - 1] = matrix[matrix.length - i - 1][j];
        matrix[matrix.length - i - 1][j] = temp;
      }
    }
    return matrix;
  }


}
